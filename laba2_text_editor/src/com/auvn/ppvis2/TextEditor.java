package com.auvn.ppvis2;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.auvn.ppvis2.model.Document;
import com.auvn.ppvis2.model.commands.Command;
import com.auvn.ppvis2.model.observer.EditDocObserver;
import com.auvn.ppvis2.model.observer.NewDocObserver;
import com.auvn.ppvis2.model.observer.OpenDocObservable;
import com.auvn.ppvis2.model.observer.OpenDocObserver;
import com.auvn.ppvis2.model.observer.SaveDocObservable;
import com.auvn.ppvis2.model.observer.SaveDocObserver;
import com.auvn.ppvis2.view.MainForm;
import com.auvn.ppvis2.view.dialogs.FontDialog;
import com.auvn.ppvis2.view.dialogs.OpenDialog;
import com.auvn.ppvis2.view.dialogs.SaveDialog;

public class TextEditor implements SaveDocObservable, OpenDocObservable,
		EditDocObserver, NewDocObserver {
	private List<SaveDocObserver> saveDocObservers;
	private List<OpenDocObserver> openDocObservers;

	private Command newCommand, saveCommand, openCommand, fontCommand,
			quitCommand;

	private Document currentDocument;

	private SaveDialog saveDialog;
	private OpenDialog openDialog;
	private FontDialog fontDialog;

	private String currentDocumentPath;
	private MainForm mainForm;

	private boolean isEdited;

	public TextEditor() {
		saveDocObservers = new ArrayList<SaveDocObserver>();
		openDocObservers = new ArrayList<OpenDocObserver>();
	}

	public void quitCommand() {
		quitCommand.execute();
	}

	public void exit() {
		System.exit(0);
	}

	public void newDocument() {
		newCommand.execute();
		mainForm.getTextPanel().setText(currentDocument.getContent());
		mainForm.getTextPanel().setTextFont(currentDocument.getFont());
		isEdited = false;
	}

	public void saveDocument() {
		boolean ret;
		currentDocument.setContent(mainForm.getTextPanel().getText());
		saveCommand.execute();
		try {
			if (currentDocument.getNameDoc() == null) {
				ret = saveDialog.show(currentDocument);
				if (ret) {
					mainForm.setTitle(currentDocument.getNameDoc());
					notifyAllSaveDocObservers();
				}
			} else {
				saveDialog.writeToFile(currentDocument);
				notifyAllSaveDocObservers();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void openDocument() {
		openCommand.execute();
		if (currentDocument.getType() != null) {
			mainForm.getTextPanel().setText(currentDocument.getContent());
			mainForm.setTitle(currentDocument.getNameDoc());
			mainForm.getTextPanel().setTextFont(currentDocument.getFont());
			notifyAllOpenDocObservers();
		}

	}

	public void changeFont() {
		fontCommand.execute();
		System.out.println(currentDocument.getFont());
		mainForm.getTextPanel().setTextFont(currentDocument.getFont());
	}

	public Document getDocument() {

		try {
			return openDialog.readFromFile(openDialog.show());
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public Document getCurrentDocument() {
		return currentDocument;
	}

	public void setCurrentDocument(Document currentDocument) {
		this.currentDocument = currentDocument;
	}

	public Command getNewCommand() {
		return newCommand;
	}

	public void setNewCommand(Command newCommand) {
		this.newCommand = newCommand;
	}

	public Command getSaveCommand() {
		return saveCommand;
	}

	public void setSaveCommand(Command saveCommand) {
		this.saveCommand = saveCommand;
	}

	public SaveDialog getSaveDialog() {
		return saveDialog;
	}

	public void setSaveDialog(SaveDialog saveDialog) {
		this.saveDialog = saveDialog;
	}

	public void addSaveDocObserver(SaveDocObserver observer) {
		saveDocObservers.add(observer);
	}

	public void removeSaveDocObserver(SaveDocObserver observer) {
		saveDocObservers.remove(observer);
	}

	public void notifyAllSaveDocObservers() {
		for (SaveDocObserver observer : saveDocObservers)
			observer.saveDocUpdate();

		isEdited = false;
	}

	public String getCurrentDocumentPath() {
		return currentDocumentPath;
	}

	public void setCurrentDocumentPath(String currentDocumentPath) {
		this.currentDocumentPath = currentDocumentPath;
	}

	public OpenDialog getOpenDialog() {
		return openDialog;
	}

	public void setOpenDialog(OpenDialog openDialog) {
		this.openDialog = openDialog;
	}

	public Command getOpenCommand() {
		return openCommand;
	}

	public void setOpenCommand(Command openCommand) {
		this.openCommand = openCommand;
	}

	public MainForm getMainForm() {
		return mainForm;
	}

	public void setMainForm(MainForm mainForm) {
		this.mainForm = mainForm;
	}

	public void addOpenDocObserver(OpenDocObserver observer) {
		openDocObservers.add(observer);
	}

	public void removeOpendocObserver(OpenDocObserver observer) {
		openDocObservers.remove(observer);
	}

	public void notifyAllOpenDocObservers() {
		for (OpenDocObserver observer : openDocObservers)
			observer.openDocUpdate();
		isEdited = false;
	}

	public Command getFontCommand() {
		return fontCommand;
	}

	public void setFontCommand(Command fontCommand) {
		this.fontCommand = fontCommand;
	}

	public FontDialog getFontDialog() {
		return fontDialog;
	}

	public void setFontDialog(FontDialog fontDialog) {
		this.fontDialog = fontDialog;
	}

	public void editDocUpdate() {
		isEdited = true;
	}

	public boolean isEdited() {
		return isEdited;
	}

	public void setEdited(boolean isEdited) {
		this.isEdited = isEdited;
	}

	public Command getQuitCommand() {
		return quitCommand;
	}

	public void setQuitCommand(Command quitCommand) {
		this.quitCommand = quitCommand;
	}

	public void newDocUpdate() {
		isEdited = true;
	}
}
