package com.auvn.ppvis2.controller;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import com.auvn.ppvis2.TextEditor;

public class MainWindowActions extends WindowAdapter {
	private TextEditor textEditor;

	public MainWindowActions(TextEditor textEditor) {
		this.textEditor = textEditor;
	}

	public void windowClosing(WindowEvent e) {
		textEditor.quitCommand();
	}
}
