package com.auvn.ppvis2.controller.menuactions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import com.auvn.ppvis2.TextEditor;
import com.auvn.ppvis2.model.observer.NewDocObservable;
import com.auvn.ppvis2.model.observer.NewDocObserver;
import com.auvn.ppvis2.view.MainForm;
import com.auvn.ppvis2.view.menubar.MainMenuBar;

public class FileMenuActions implements ActionListener, NewDocObservable {
	private List<NewDocObserver> newDocObservers;
	private TextEditor textEditor;
	private MainMenuBar mainMenuBar;

	public FileMenuActions(TextEditor textEditor, MainForm mainForm) {
		this.textEditor = textEditor;
		this.mainMenuBar = mainForm.getMainMenuBar();

		newDocObservers = new ArrayList<NewDocObserver>();
	}

	public void actionPerformed(ActionEvent e) {
		Object clickedObj = e.getSource();
		if (clickedObj == mainMenuBar.getNewMenuItem()) {
			textEditor.newDocument();
			notifyAllNewDocObservers();
		} else if (clickedObj == mainMenuBar.getSaveMenuItem()) {

			textEditor.saveDocument();
		} else if (clickedObj == mainMenuBar.getSaveAsMenuItem()) {
			textEditor.getCurrentDocument().setNameDoc(null);
			textEditor.saveDocument();

		} else if (clickedObj == mainMenuBar.getOpenMenuItem()) {
			textEditor.openDocument();
		} else if (clickedObj == mainMenuBar.getFontEditMenuItem()) {
			textEditor.changeFont();
		} else if (clickedObj == mainMenuBar.getExitMenuItem()) {
			textEditor.quitCommand();
		}

	}

	public void addNewDocObserver(NewDocObserver observer) {
		newDocObservers.add(observer);
	}

	public void notifyAllNewDocObservers() {
		for (NewDocObserver observer : newDocObservers)
			observer.newDocUpdate();
	}

	public void removeNewDocObserver(NewDocObserver observer) {
		newDocObservers.remove(observer);
	}

}
