package com.auvn.ppvis2.controller;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.List;

import com.auvn.ppvis2.model.observer.EditDocObservable;
import com.auvn.ppvis2.model.observer.EditDocObserver;

public class TextEditorActions implements KeyListener, EditDocObservable {
	private List<EditDocObserver> editDocObservers;

	public TextEditorActions() {

		editDocObservers = new ArrayList<EditDocObserver>();
	}

	public void keyPressed(KeyEvent arg0) {

	}

	public void keyReleased(KeyEvent e) {

	}

	public void keyTyped(KeyEvent e) {
		notifyAllEditObservers();
	}

	public void addEditDocObserver(EditDocObserver observer) {
		editDocObservers.add(observer);
	}

	public void notifyAllEditObservers() {
		for (EditDocObserver observer : editDocObservers)
			observer.editDocUpdate();
	}

	public void removeEditDocObserver(EditDocObserver observer) {
		editDocObservers.remove(observer);
	}

}
