package com.auvn.ppvis2.view;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;

import com.auvn.ppvis2.view.menubar.MainMenuBar;
import com.auvn.ppvis2.view.panels.TextPanel;

public class MainForm extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2105340667483576954L;
	private MainMenuBar mainMenuBar;
	private TextPanel textPanel;

	public MainForm(MainMenuBar mainMenuBar, TextPanel textPanel) {
		setProperties();
		this.mainMenuBar = mainMenuBar;
		this.textPanel = textPanel;

		setContentPane(textPanel);

		this.setJMenuBar(mainMenuBar);

	}

	private void setProperties() {
		setTitle("Text editor");
		setSize(new Dimension(600, 500));

		setLocation((int) Toolkit.getDefaultToolkit().getScreenSize()
				.getWidth()
				/ 2 - getWidth() / 2, (int) Toolkit.getDefaultToolkit()
				.getScreenSize().getHeight()
				/ 2 - getHeight() / 2);
	}

	public MainMenuBar getMainMenuBar() {
		return mainMenuBar;
	}

	public void setMainMenuBar(MainMenuBar mainMenuBar) {
		this.mainMenuBar = mainMenuBar;
	}

	public TextPanel getTextPanel() {
		return textPanel;
	}

	public void setTextPanel(TextPanel textPanel) {
		this.textPanel = textPanel;
	}

}
