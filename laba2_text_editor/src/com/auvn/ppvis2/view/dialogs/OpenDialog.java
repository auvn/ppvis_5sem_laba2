package com.auvn.ppvis2.view.dialogs;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;

import javax.swing.JFileChooser;

import com.auvn.ppvis2.model.Document;

public class OpenDialog {
	private JFileChooser openFileChooser;

	public OpenDialog() {
		openFileChooser = new JFileChooser("./");
	}

	public String show() {
		int ret = openFileChooser.showOpenDialog(null);
		if (ret == JFileChooser.APPROVE_OPTION) {
			return openFileChooser.getSelectedFile().getAbsolutePath();
		}
		return null;
	}

	public Document readFromFile(String fileName) throws FileNotFoundException,
			IOException, ClassNotFoundException {
		ObjectInputStream documentInputStream = new ObjectInputStream(
				new FileInputStream(new File(fileName)));
		Document loadedDocument = (Document) documentInputStream.readObject();
		documentInputStream.close();
		loadedDocument.setNameDoc(fileName);
		return loadedDocument;
	}
}
