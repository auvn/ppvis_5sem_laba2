package com.auvn.ppvis2.view.dialogs;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import com.auvn.ppvis2.model.Document;

public class SaveDialog {
	private JFileChooser saveFileChooser;
	private File selectedFile;

	public SaveDialog() {
		saveFileChooser = new JFileChooser("./");
	}

	public boolean show(Document document) throws FileNotFoundException,
			IOException {
		int ret = saveFileChooser.showSaveDialog(null);
		if (ret == JFileChooser.APPROVE_OPTION) {
			if ((selectedFile = saveFileChooser.getSelectedFile()).isFile()) {
				if ((JOptionPane.showConfirmDialog(saveFileChooser,
						"Overwrite the existing file?", "Message",
						JOptionPane.YES_NO_OPTION)) == JOptionPane.YES_OPTION) {
					writeToFile(document);
					return true;
				} else
					return show(document);
			} else {
				writeToFile(document);
				return true;
			}
		}
		return false;
	}

	public void writeToFile(Document document) throws FileNotFoundException,
			IOException {
		if (document.getNameDoc() != null)
			selectedFile = new File(document.getNameDoc());
		ObjectOutputStream documentOutputStream = new ObjectOutputStream(
				new FileOutputStream(selectedFile));
		document.setNameDoc(selectedFile.getAbsolutePath());
		documentOutputStream.writeObject(document);
		documentOutputStream.close();
	}
}
