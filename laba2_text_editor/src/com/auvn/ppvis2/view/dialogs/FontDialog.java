package com.auvn.ppvis2.view.dialogs;

import java.awt.Font;

import com.auvn.ppvis2.view.JFontChooser;

public class FontDialog {
	private JFontChooser fontChooser;

	public FontDialog() {
		fontChooser = new JFontChooser();
	}

	public Font show() {
		int ret = fontChooser.showDialog(null);
		if (ret == JFontChooser.OK_OPTION) {
			return fontChooser.getSelectedFont();
		}
		return null;
	}
}
