package com.auvn.ppvis2.view.panels;

import java.awt.BorderLayout;
import java.awt.Font;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import com.auvn.ppvis2.model.observer.NewDocObserver;
import com.auvn.ppvis2.model.observer.OpenDocObserver;

public class TextPanel extends JPanel implements NewDocObserver,
		OpenDocObserver {

	private static final long serialVersionUID = -2237897363038181139L;
	private JTextArea textArea;

	public TextPanel() {
		setLayout(new BorderLayout());

		textArea = new JTextArea();
		textArea.setVisible(false);
		textArea.setWrapStyleWord(true);
		textArea.setLineWrap(true);

		add(new JScrollPane(textArea, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER), BorderLayout.CENTER);

	}

	public JTextArea getTextArea() {
		return textArea;
	}

	public void setTextArea(JTextArea textArea) {
		this.textArea = textArea;
	}

	public String getText() {
		return textArea.getText();
	}

	public void setText(String text) {
		textArea.setText(text);
	}

	public void setTextFont(Font font) {
		textArea.setFont(font);
	}

	public void newDocUpdate() {
		textArea.setVisible(true);
	}

	public void openDocUpdate() {
		textArea.setVisible(true);
	}

}
