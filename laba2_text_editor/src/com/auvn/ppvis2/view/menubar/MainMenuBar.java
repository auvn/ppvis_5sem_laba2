package com.auvn.ppvis2.view.menubar;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import com.auvn.ppvis2.model.observer.EditDocObserver;
import com.auvn.ppvis2.model.observer.NewDocObserver;
import com.auvn.ppvis2.model.observer.OpenDocObserver;
import com.auvn.ppvis2.model.observer.SaveDocObserver;

public class MainMenuBar extends JMenuBar implements NewDocObserver,
		SaveDocObserver, OpenDocObserver, EditDocObserver {
	private static final long serialVersionUID = 5348150556502120756L;

	private JMenu fileMenu;
	private JMenu editMenu;

	private JMenuItem newMenuItem;
	private JMenuItem openMenuItem;
	private JMenuItem saveMenuItem;
	private JMenuItem saveAsMenuItem;
	private JMenuItem exitMenuItem;

	private JMenuItem fontEditMenuItem;

	public MainMenuBar() {
		fileMenu = new JMenu("File");
		editMenu = new JMenu("Edit");

		newMenuItem = new JMenuItem("New");
		openMenuItem = new JMenuItem("Open");
		saveMenuItem = new JMenuItem("Save");
		saveMenuItem.setEnabled(false);
		saveAsMenuItem = new JMenuItem("Save As");
		saveAsMenuItem.setEnabled(false);
		exitMenuItem = new JMenuItem("Exit");

		fontEditMenuItem = new JMenuItem("Font...");
		fontEditMenuItem.setEnabled(false);

		fileMenu.add(newMenuItem);
		fileMenu.add(openMenuItem);
		fileMenu.add(saveMenuItem);
		fileMenu.add(saveAsMenuItem);
		fileMenu.addSeparator();
		fileMenu.add(exitMenuItem);

		editMenu.add(fontEditMenuItem);

		add(fileMenu);
		add(editMenu);
	}

	public JMenu getFileMenu() {
		return fileMenu;
	}

	public void setFileMenu(JMenu fileMenu) {
		this.fileMenu = fileMenu;
	}

	public JMenuItem getNewMenuItem() {
		return newMenuItem;
	}

	public void setNewMenuItem(JMenuItem newMenuItem) {
		this.newMenuItem = newMenuItem;
	}

	public JMenuItem getOpenMenuItem() {
		return openMenuItem;
	}

	public void setOpenMenuItem(JMenuItem openMenuItem) {
		this.openMenuItem = openMenuItem;
	}

	public JMenuItem getSaveMenuItem() {
		return saveMenuItem;
	}

	public void setSaveMenuItem(JMenuItem saveMenuItem) {
		this.saveMenuItem = saveMenuItem;
	}

	public JMenuItem getSaveAsMenuItem() {
		return saveAsMenuItem;
	}

	public void setSaveAsMenuItem(JMenuItem saveAsMenuItem) {
		this.saveAsMenuItem = saveAsMenuItem;
	}

	public JMenuItem getExitMenuItem() {
		return exitMenuItem;
	}

	public void setExitMenuItem(JMenuItem exitMenuItem) {
		this.exitMenuItem = exitMenuItem;
	}

	public void newDocUpdate() {
		saveMenuItem.setEnabled(true);
		saveAsMenuItem.setEnabled(true);
		fontEditMenuItem.setEnabled(true);
	}

	public void saveDocUpdate() {
		saveMenuItem.setEnabled(false);
	}

	public void openDocUpdate() {
		saveMenuItem.setEnabled(false);
		saveAsMenuItem.setEnabled(true);
		fontEditMenuItem.setEnabled(true);
	}

	public void editDocUpdate() {
		saveMenuItem.setEnabled(true);
		saveAsMenuItem.setEnabled(true);
	}

	public JMenuItem getFontEditMenuItem() {
		return fontEditMenuItem;
	}

	public void setFontEditMenuItem(JMenuItem fontEditMenuItem) {
		this.fontEditMenuItem = fontEditMenuItem;
	}

}
