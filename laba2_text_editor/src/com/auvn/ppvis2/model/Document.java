package com.auvn.ppvis2.model;

import java.awt.Font;
import java.io.Serializable;

public class Document implements Serializable {

	private static final long serialVersionUID = 7831226558457729140L;

	private String nameDoc;
	private String type;
	private Font font;

	private String content;

	public void changeFont() {

	}

	public void copy() {

	}

	public void draw() {

	}

	public void enter() {

	}

	public void font(Font font) {
		if (font != null)
			this.font = font;
	}

	public Document newDocument() {
		this.nameDoc = null;
		this.font = new Font("Arial", Font.PLAIN, 14);
		this.type = "dc";
		this.content = "";
		return null;
	}

	public void openDocument(Document document) {
		if (document != null) {
			setDocument(document);
		} else {
			this.type = null;
		}
	}

	private void setDocument(Document document) {
		this.content = document.getContent();
		this.nameDoc = document.getNameDoc();
		this.font = document.getFont();
		this.type = document.getType();
	}

	public void paste() {

	}

	public void save() {

	}

	public String getNameDoc() {
		return nameDoc;
	}

	public void setNameDoc(String nameDoc) {
		this.nameDoc = nameDoc;
	}

	public Font getFont() {
		return font;
	}

	public void setFont(Font font) {
		this.font = font;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

}
