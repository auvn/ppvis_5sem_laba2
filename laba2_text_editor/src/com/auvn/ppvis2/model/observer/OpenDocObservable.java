package com.auvn.ppvis2.model.observer;

public interface OpenDocObservable {
	public void addOpenDocObserver(OpenDocObserver observer);

	public void removeOpendocObserver(OpenDocObserver observer);

	public void notifyAllOpenDocObservers();
}
