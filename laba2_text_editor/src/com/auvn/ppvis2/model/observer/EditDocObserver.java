package com.auvn.ppvis2.model.observer;

public interface EditDocObserver {
	public void editDocUpdate();
}
