package com.auvn.ppvis2.model.observer;

public interface EditDocObservable {
	public void addEditDocObserver(EditDocObserver observer);

	public void removeEditDocObserver(EditDocObserver observer);

	public void notifyAllEditObservers();
}
