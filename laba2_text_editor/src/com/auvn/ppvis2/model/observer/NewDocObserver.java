package com.auvn.ppvis2.model.observer;

public interface NewDocObserver {
	public void newDocUpdate();
}
