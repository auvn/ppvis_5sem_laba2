package com.auvn.ppvis2.model.observer;

public interface OpenDocObserver {
	public void openDocUpdate();
}
