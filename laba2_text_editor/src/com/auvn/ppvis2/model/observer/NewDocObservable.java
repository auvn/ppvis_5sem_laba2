package com.auvn.ppvis2.model.observer;

public interface NewDocObservable {
	public void addNewDocObserver(NewDocObserver observer);

	public void removeNewDocObserver(NewDocObserver observer);

	public void notifyAllNewDocObservers();
}
