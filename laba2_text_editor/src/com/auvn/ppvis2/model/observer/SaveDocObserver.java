package com.auvn.ppvis2.model.observer;

public interface SaveDocObserver {
	public void saveDocUpdate();
}
