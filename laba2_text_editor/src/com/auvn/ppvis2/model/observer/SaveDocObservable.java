package com.auvn.ppvis2.model.observer;

public interface SaveDocObservable {
	public void addSaveDocObserver(SaveDocObserver observer);

	public void removeSaveDocObserver(SaveDocObserver observer);

	public void notifyAllSaveDocObservers();
}
