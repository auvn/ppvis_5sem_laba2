package com.auvn.ppvis2.model.commands;

import com.auvn.ppvis2.model.Document;

public class NewCommand implements Command {
	private Document document;

	public NewCommand(Document document) {
		this.document = document;
	}

	public void execute() {
		document.newDocument();
		System.out.println("New document created");
	}

	public void revert() {

	}

}
