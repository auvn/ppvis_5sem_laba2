package com.auvn.ppvis2.model.commands;

import javax.swing.JOptionPane;

import com.auvn.ppvis2.TextEditor;

public class QuitCommand implements Command {
	private TextEditor textEditor;

	public QuitCommand(TextEditor textEditor) {
		this.textEditor = textEditor;
	}

	public void execute() {
		if (textEditor.isEdited()) {
			int ret = JOptionPane.showConfirmDialog(null,
					"Do you want to close your file without saving?",
					"Message", JOptionPane.YES_NO_OPTION,
					JOptionPane.INFORMATION_MESSAGE);
			if (ret == JOptionPane.YES_OPTION) {
				textEditor.saveDocument();
				textEditor.exit();
			} else if (ret == JOptionPane.NO_OPTION)
				textEditor.exit();

		} else
			textEditor.exit();
	}

	public void revert() {

	}
}
