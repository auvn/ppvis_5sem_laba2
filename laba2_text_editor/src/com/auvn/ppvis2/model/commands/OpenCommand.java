package com.auvn.ppvis2.model.commands;

import com.auvn.ppvis2.TextEditor;
import com.auvn.ppvis2.model.Document;

public class OpenCommand implements Command {
	private Document document;
	private TextEditor textEditor;

	public OpenCommand(Document document, TextEditor textEditor) {
		this.document = document;
		this.textEditor = textEditor;
	}

	public void execute() {

		document.openDocument(textEditor.getDocument());
	}

	public void revert() {
	};
}
