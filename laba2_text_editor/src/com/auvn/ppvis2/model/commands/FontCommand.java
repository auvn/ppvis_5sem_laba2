package com.auvn.ppvis2.model.commands;

import com.auvn.ppvis2.TextEditor;
import com.auvn.ppvis2.model.Document;

public class FontCommand implements Command {
	private Document document;
	private TextEditor textEditor;

	public FontCommand(Document document, TextEditor textEditor) {
		this.document = document;
		this.textEditor = textEditor;
	}

	public void execute() {
		document.font(textEditor.getFontDialog().show());
	}

	public void revert() {

	}
}
