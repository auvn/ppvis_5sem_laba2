package com.auvn.ppvis2.model.commands;

import com.auvn.ppvis2.model.Document;

public class SaveCommand implements Command {
	private Document document;

	public SaveCommand(Document document) {
		this.document = document;
	}

	public void revert() {

	}

	public void execute() {
		document.save();
		System.out.println("Document has been saved");
	}
}
