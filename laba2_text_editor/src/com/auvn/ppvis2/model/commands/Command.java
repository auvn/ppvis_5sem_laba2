package com.auvn.ppvis2.model.commands;

public interface Command {
	public void execute();
	public void revert();
}
