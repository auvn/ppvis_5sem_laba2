package com.auvn.ppvis2;


import com.auvn.ppvis2.controller.MainWindowActions;
import com.auvn.ppvis2.controller.TextEditorActions;
import com.auvn.ppvis2.controller.menuactions.FileMenuActions;
import com.auvn.ppvis2.model.Document;
import com.auvn.ppvis2.model.commands.FontCommand;
import com.auvn.ppvis2.model.commands.NewCommand;
import com.auvn.ppvis2.model.commands.OpenCommand;
import com.auvn.ppvis2.model.commands.QuitCommand;
import com.auvn.ppvis2.model.commands.SaveCommand;
import com.auvn.ppvis2.view.MainForm;
import com.auvn.ppvis2.view.dialogs.FontDialog;
import com.auvn.ppvis2.view.dialogs.OpenDialog;
import com.auvn.ppvis2.view.dialogs.SaveDialog;
import com.auvn.ppvis2.view.menubar.MainMenuBar;
import com.auvn.ppvis2.view.panels.TextPanel;

public class Run {
	private static void init() {
		MainMenuBar mainMenuBar = new MainMenuBar();
		TextPanel textPanel = new TextPanel();

		MainForm mainForm = new MainForm(mainMenuBar, textPanel);

		Document rootDocument = new Document();

		TextEditor textEditor = new TextEditor();

		NewCommand newCommand = new NewCommand(rootDocument);
		SaveCommand saveCommand = new SaveCommand(rootDocument);
		OpenCommand openCommand = new OpenCommand(rootDocument, textEditor);
		FontCommand fontCommand = new FontCommand(rootDocument, textEditor);
		QuitCommand quitCommand = new QuitCommand(textEditor);

		SaveDialog saveDialog = new SaveDialog();
		OpenDialog openDialog = new OpenDialog();
		FontDialog fontDialog = new FontDialog();

		FileMenuActions fileMenuActions = new FileMenuActions(textEditor,
				mainForm);
		TextEditorActions textEditorActions = new TextEditorActions();
		MainWindowActions mainWindowActions = new MainWindowActions(textEditor);

		textEditorActions.addEditDocObserver(mainMenuBar);

		fileMenuActions.addNewDocObserver(textPanel);
		fileMenuActions.addNewDocObserver(mainMenuBar);
		fileMenuActions.addNewDocObserver(textEditor);

		textEditor.setCurrentDocument(rootDocument);

		textEditor.setNewCommand(newCommand);
		textEditor.setSaveCommand(saveCommand);
		textEditor.setOpenCommand(openCommand);
		textEditor.setFontCommand(fontCommand);
		textEditor.setQuitCommand(quitCommand);

		textEditor.setSaveDialog(saveDialog);
		textEditor.setOpenDialog(openDialog);
		textEditor.setFontDialog(fontDialog);

		textEditor.addSaveDocObserver(mainMenuBar);
		textEditor.addOpenDocObserver(textPanel);
		textEditor.addOpenDocObserver(mainMenuBar);

		textEditor.setMainForm(mainForm);

		mainMenuBar.getNewMenuItem().addActionListener(fileMenuActions);
		mainMenuBar.getSaveMenuItem().addActionListener(fileMenuActions);
		mainMenuBar.getSaveAsMenuItem().addActionListener(fileMenuActions);
		mainMenuBar.getOpenMenuItem().addActionListener(fileMenuActions);
		mainMenuBar.getFontEditMenuItem().addActionListener(fileMenuActions);
		mainMenuBar.getExitMenuItem().addActionListener(fileMenuActions);

		mainForm.getTextPanel().getTextArea().addKeyListener(textEditorActions);
		mainForm.addWindowListener(mainWindowActions);
		mainForm.setVisible(true);

	}

	public static void main(String[] args) {
		init();
	}
}
